export const icons = {
  back: require("./arrow-left.png"),
  bullet: require("./bullet.png"),
  bug: require("./ladybug.png"),
  home: require("./home.png"),
  notification: require("./notification.png"),
  account: require("./account.png"),
  message: require("./message.png"),
}

export type IconTypes = keyof typeof icons
