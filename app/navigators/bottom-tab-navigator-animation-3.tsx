import React, { useEffect, useRef } from 'react'
import { NotificationScreen, MessageScreen } from "../screens"
import { useColorScheme, Text} from "react-native"
import { StyleSheet,TouchableOpacity,View} from "react-native"
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import {AppStack, Account_Stack,canExit} from "./index";
import Icon_animation,{Icons_animation} from "../components/icon/icon.animation";
import * as Animatable from 'react-native-animatable';
import Colors from '../components/Colors/Colors';

// Bottom tab Navigator
// style
const styles = StyleSheet.create({
  tabbar:{
      height: 60,
      position: 'absolute',
      bottom: 16,
      right: 16,
      left: 16,
      borderRadius: 16
  },
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    borderRadius: 16,
  }
})
  
// ParamList
export type FooterNavigatorParamList = {
    Home: undefined
    Notification: undefined
    Account: undefined
    Message: undefined
}

// Animation 
const TabArr = [
  { route: 'Home', label: 'Home', type: Icons_animation.Ionicons, icon: 'home', component: AppStack,color: Colors.green, alphaClr: Colors.greenAlpha },
  { route: 'Notification', label: 'Notification', type: Icons_animation.Ionicons, icon: 'ios-notifications-sharp', component: NotificationScreen,color: Colors.primary, alphaClr: Colors.primaryAlpha  },
  { route: 'Account', label: 'Account', type: Icons_animation.MaterialCommunityIcons, icon: 'account-circle', component: Account_Stack,color: Colors.red, alphaClr: Colors.redAlpha },
  { route: 'Message', label: 'Message', type: Icons_animation.MaterialCommunityIcons, icon: 'message-processing', component: MessageScreen,color: Colors.purple, alphaClr: Colors.purpleAlpha },
];


const animate1 = { 0: { scale: 0 }, 1: { scale: 1 } }
const animate2 = { 0: { scale: 1, },1: { scale: 0} }

const text_animate1 = {0: {scale: 0}, 1: {scale: 1}}
const text_animate2 = {0: {scale: 1}, 1: {scale: 0}}

const TabButton = (props) => {
  const { item, onPress, accessibilityState } = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const textViewRef = useRef(null);

  useEffect(() => {
    if (focused) { 
      viewRef.current.animate(animate1);
      textViewRef.current.animate(text_animate1);
    } else {
      viewRef.current.animate(animate2);
      textViewRef.current.animate(text_animate2);
    }
  }, [focused])

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={[styles.container, {flex: focused ? 1 : 0.65}]}>
      <View>
        <Animatable.View
          ref={viewRef}
          style={[StyleSheet.absoluteFill, { backgroundColor: item.color, borderRadius: 16 }]} />
        <View style={[styles.btn, { backgroundColor: focused ? null : item.alphaClr }]}>
          <Icon_animation type={item.type} name={item.icon} color={focused ? Colors.white : Colors.primary} size={24} style={{}} />
          <Animatable.View
            ref={textViewRef}>
            {focused && <Text style={{
              color: Colors.white, paddingHorizontal: 8
            }}>{item.label}</Text>}
          </Animatable.View>
        </View>
      </View>
    </TouchableOpacity>
  )
}
  
// Footer
const Tab = createBottomTabNavigator<FooterNavigatorParamList>()
const Bottom_Tabs = () => {
    return(
    <Tab.Navigator
        screenOptions={{
            headerShown: false,
            tabBarHideOnKeyboard: true,
            tabBarShowLabel: false,
            tabBarStyle: {
                ...styles.tabbar,
            },
        }}
        initialRouteName="Home"
    >
        
    {TabArr.map((item, index) => {
        return (
          <Tab.Screen key={index} name={item.route} component={item.component}
            options={{
              tabBarButton: (props) => <TabButton {...props} item={item} />
            }}
          />
        )
    })}
  </Tab.Navigator>
  )
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}
  
export const AppNavigator = (props: NavigationProps) => {
    const colorScheme = useColorScheme()
    useBackButtonHandler(canExit)
    return (
      <NavigationContainer
        ref={navigationRef}
        theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
        {...props}
      >
  
      <Bottom_Tabs />
      </NavigationContainer>
    )
  }
  
  AppNavigator.displayName = "AppNavigator"