
import React from "react"
import { NotificationScreen, MessageScreen } from "../screens"
import { useColorScheme} from "react-native"
import { ViewStyle,ImageStyle,TextStyle,StyleSheet, View, Image,Text} from "react-native"
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import {AppStack, Account_Stack,canExit} from "./index";
import {icons} from "../components/icon/icons"

// Bottom tab Navigator
// style
const styles = StyleSheet.create({
    tabbar:{
        flex: 1,
        position: 'absolute',
        height: 50
    }
})
const VIEW_FOOTER: ViewStyle = {
  alignItems: 'center',
  justifyContent: 'center',
}
const IMAGE_FOOTER: ImageStyle = {
  width: 25,
  height: 25,   
}
const TEXT_FOOTER: TextStyle = {
  fontSize:15,
}

// ParamList
export type FooterNavigatorParamList = {
    home: undefined
    notification: undefined
    account: undefined
    message: undefined
  }
  
// Footer
const Tab = createBottomTabNavigator<FooterNavigatorParamList>()
const Bottom_Tabs = () => {
  return(
  <Tab.Navigator
    screenOptions={{
        headerShown: false,
        tabBarHideOnKeyboard: true,
        tabBarShowLabel: false,
        tabBarStyle: {
            ...styles.tabbar,
        },
      }}
    initialRouteName="home"
  >
      <Tab.Screen name="home" component={AppStack} options={{
        tabBarIcon:({focused}) => (
        <View style={VIEW_FOOTER}>
          <Image source={icons.home} style={{...IMAGE_FOOTER,tintColor: focused  ? '#e32f45' : '#748c94'}}/>
          <Text style={{...TEXT_FOOTER,color: focused  ? '#e32f45' : '#748c94'}}>Home</Text>
        </View>
        )}}
      />
      <Tab.Screen name="notification" component={NotificationScreen} options={{
        tabBarIcon:({focused}) => (
          <View style={VIEW_FOOTER}>
            <Image source={icons.notification} style={{...IMAGE_FOOTER,tintColor: focused  ? '#e32f45' : '#748c94'}}/>
            <Text style={{...TEXT_FOOTER,color: focused  ? '#e32f45' : '#748c94'}}>Notification</Text>
          </View>
        )}}
      />
      <Tab.Screen name="account" component={Account_Stack} options={{
        tabBarIcon:({focused}) => (
          <View style={VIEW_FOOTER}>
            <Image source={icons.account} style={{...IMAGE_FOOTER,tintColor: focused  ? '#e32f45' : '#748c94'}}/>
            <Text style={{...TEXT_FOOTER,color: focused  ? '#e32f45' : '#748c94'}}>Account</Text>
          </View>
        )}}
      />
      <Tab.Screen name="message" component={MessageScreen} options={{
        tabBarIcon:({focused}) => (
          <View style={VIEW_FOOTER}>
            <Image source={icons.message} style={{...IMAGE_FOOTER,tintColor: focused  ? '#e32f45' : '#748c94'}}/>
            <Text style={{...TEXT_FOOTER,color: focused  ? '#e32f45' : '#748c94'}}>Message</Text>
          </View>
        )}}
        />
  </Tab.Navigator>
  )
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}
  
export const AppNavigator = (props: NavigationProps) => {
    const colorScheme = useColorScheme()
    useBackButtonHandler(canExit)
    return (
      <NavigationContainer
        ref={navigationRef}
        theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
        {...props}
      >
  
      <Bottom_Tabs />
      </NavigationContainer>
    )
  }
  
  AppNavigator.displayName = "AppNavigator"