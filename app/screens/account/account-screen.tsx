import React, { FC,useEffect } from "react"
import { View, ViewStyle, TextStyle,Image, ImageStyle, TouchableHighlight } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Screen,
  Text,
  GradientBackground,
} from "../../components"
import { color} from "../../theme"
import { AccountNavigatorParamList } from "../../navigators"
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as Animatable from 'react-native-animatable'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const TITLE: TextStyle = {
  fontSize: 24,
  fontWeight: 'bold',
  marginTop:10
}
const CAPTION: TextStyle = {
  fontSize: 14,
  lineHeight: 14,
  fontWeight: '500',
  marginTop:5
}
const VIEW_AVATAR: ViewStyle = {
  flexDirection: 'row',
  marginTop: 25,
  paddingHorizontal: 10
}
const AVATAR: ImageStyle = {
  width: 80,
  height: 80,
  borderRadius: 75,
  borderWidth:2,
}
const VIEW_ICON: ViewStyle = {
  marginLeft:10,
  flexDirection: 'row',
  marginTop: 10,
  paddingHorizontal: 20
}
const TEXT_ICON: TextStyle = {
  marginLeft: 10,
  fontSize: 14
}
const VIEW_TOUCHABLE_ITEM: ViewStyle = {
  // paddingTop: 5,
  // paddingBottom: 5,
}
const VIEW_ITEM: ViewStyle = {
  flexDirection: 'row',
  paddingVertical: 12,
  paddingHorizontal: 20,
  borderBottomWidth:1,
  borderBottomColor: "#ffffff",
}
const MENU_ITEM_TEXT: TextStyle = {
  marginLeft: 20,
  fontWeight: '600',
  fontSize: 16,
  lineHeight: 26,
}


export const AcountScreen: FC<StackScreenProps<AccountNavigatorParamList, "account_home">> = observer(
  ({ navigation }) => {
  
    return (
      <Animatable.View testID="AcountScreen" style={FULL} >
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} preset="scroll" backgroundColor={color.transparent}>
          
          {/* Header gonna be here */}
          {/* <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text="Header need here" />
          </Text> */}

          {/* Avatar */}
          <View style={VIEW_AVATAR}>
            <Image style={AVATAR} resizeMode="cover" source={require('../account/avatar.png')} />
            <View style={{marginLeft: 20}}>
              <Text style={TITLE}>Phan Minh Phú</Text>
              <Text style={CAPTION}>@phanminhphu159</Text>
            </View>
            <Icon name="account-edit" color="#FF6347" size={25} style={{marginLeft:'auto'}}/>
          </View>

          {/* Some infomation to contact */}
          <View style={{...VIEW_ICON, marginTop: 15}}>
            <Icon name="map-marker-radius" color="#ffffff" size={20}/>
            <Text style={TEXT_ICON}>Đà Nẵng, Việt Nam</Text>
          </View>
          <View style={VIEW_ICON}>
          <Icon name="phone" color="#ffffff" size={20}/>
            <Text style={TEXT_ICON}>0905693609</Text>
          </View>
          <View style={VIEW_ICON}>
            <Icon name="email" color="#ffffff" size={20}/>
            <Text style={TEXT_ICON}>phanminhphu159@gmail.com</Text>
          </View>
          
          {/* Touchable Icon + Text to see details */}
          <View style={{marginTop:100}}/>
          <TouchableHighlight style={{ borderTopWidth:1, borderTopColor: "#ffffff",}} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Icon name="heart-outline" color="#FF6347" size={25}/>
              <Text style={MENU_ITEM_TEXT}>Your Favorites</Text>
              <Icon name="chevron-right" color="#FF6347" size={25} style={{marginLeft: "auto"}}/>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Icon name="credit-card" color="#FF6347" size={25}/>
              <Text style={MENU_ITEM_TEXT}>Payment</Text>
              <Icon name="chevron-right" color="#FF6347" size={25} style={{marginLeft: "auto"}}/>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Icon name="share-outline" color="#FF6347" size={25}/>
              <Text style={MENU_ITEM_TEXT}>Tell Your Friends</Text>
              <Icon name="chevron-right" color="#FF6347" size={25} style={{marginLeft: "auto"}}/>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Icon name="account-check-outline" color="#FF6347" size={25}/>
              <Text style={MENU_ITEM_TEXT}>Support</Text>
              <Icon name="chevron-right" color="#FF6347" size={25} style={{marginLeft: "auto"}}/>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={VIEW_TOUCHABLE_ITEM} onPress={() => {}}>
            <View style={VIEW_ITEM}>
              <Icon name="cog-outline" color="#FF6347" size={25}/>
              <Text style={MENU_ITEM_TEXT}>Settings</Text>
              <Icon name="chevron-right" color="#FF6347" size={25} style={{marginLeft: "auto"}}/>
            </View>
          </TouchableHighlight>
        </Screen>
      </Animatable.View>
    )
  },
)