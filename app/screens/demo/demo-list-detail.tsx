import React, { FC } from "react"
import { View, ViewStyle, TextStyle, ImageStyle} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Header,
  Screen,
  Text,
  GradientBackground,
  AutoImage as Image,
} from "../../components"
import { color, spacing, typography } from "../../theme"
import { NavigatorParamList } from "../../navigators"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[4] + spacing[1],
  paddingHorizontal: 0,
}
const HEADER_TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}
const TITLE_WRAPPER: TextStyle = {
  ...TEXT,
  textAlign: "center",
}
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
}
const BOWSER: ImageStyle = {
  alignSelf: "center",
  marginVertical: spacing[5],
  maxWidth: "100%",
  width: 343,
  height: 230,
}


export const DemoListDetail: FC<StackScreenProps<NavigatorParamList, "demoListDetail">> = observer(
  ({ navigation,route }) => {
    
    const goBack = () => navigation.goBack()
    const item = route.params;

    return (
      <View testID="DemoListDetail" style={FULL}>
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} preset="scroll" backgroundColor={color.transparent}>
          <Header 
            headerTx="welcomeScreen.poweredBy" 
            style={HEADER} 
            leftIcon="back"
            onLeftPress={goBack}
            titleStyle={HEADER_TITLE} 
          />

          <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text="Detail Item....!" />
          </Text>

          <Image source={{ uri: item.image }} style={BOWSER} />

          <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text={item.name} />
          </Text>
          <Text style={TITLE_WRAPPER}>
            <Text style={TITLE} text={item.status} />
          </Text>
          
        </Screen>
      </View>
    )
  },
)
