import React, { useState, useEffect, FC } from "react"
import { Animated, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity ,TextInput} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { NavigatorParamList } from "../../navigators"

const FULL: ViewStyle = {
  flex: 1,
}
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
}
const HEADER: TextStyle = {
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: spacing[4],
  paddingTop: spacing[3],
}
const INPUT_CONTAINER: TextStyle = {
  flex: 2,
  borderRadius: 10,
  alignItems: "flex-start",
  backgroundColor: color.palette.deepPurple,
  width: "auto"
}
const HEADER_TITLE: TextStyle = {
  fontSize: 12,
  fontWeight: "bold",
  letterSpacing: 1.5,
  lineHeight: 15,
  textAlign: "center",
}
const LIST_CONTAINER: ViewStyle = {
  alignItems: "center",
  flexDirection: "row",
  backgroundColor: color.palette.deepPurple,
  padding: 25,
  marginBottom:10,
  borderRadius:16,
  height: 140
  // shadowColor: "#000",
  // shadowOffset:{
  //   width:0,
  //   height:10
  // },
  // shadowOpacity:1,
  // shadowRadius:20,
}
const TEXT_INPUT: TextStyle = {
  flex: 1, 
  fontSize: 18 , 
  color: "#FFFFFF",
}

const IMAGE: ImageStyle = {
  borderRadius: 35,
  height: 65,
  width: 65,
}
const LIST_TEXT: TextStyle = {
  marginLeft: 10,
}
const FLAT_LIST: ViewStyle = {
  paddingHorizontal: spacing[4]
}

export const DemoListScreen: FC<StackScreenProps<NavigatorParamList, "demoList">> = observer(
  ({ navigation }) => {

    const [searchText, setSearchText] = useState("");
    const { characterStore } = useStores() // lấy item == character
    const { characters } = characterStore

    const ListDetail = (item) => navigation.navigate("demoListDetail",item)
    const goBack = () => navigation.goBack()

    // Animation Variable
    const scrollY = React.useRef(new Animated.Value(0)).current;
    const SPACING  = 10 ; // spacing between animation item in flat list
    const HEIGHT_ITEM = 140 ; // Height of item in flat list
    const ITEM_SIZE = HEIGHT_ITEM + SPACING ; // size of item

    // Fect Data
    useEffect(() => {
      async function fetchData() {
        await characterStore.getCharacters()
      }
      fetchData()
    }, [])

    // function
    const search = (text) => {
      let filteredData = characters.filter((item) => {
      return item.name.toLowerCase().includes(text);});
      if(text == '' || null){
        return characters;
      }
      else{return filteredData;}
    }


    return (
      <View testID="DemoListScreen" style={FULL}>
        <GradientBackground colors={["#422443", "#281b34"]} />
        <Screen style={CONTAINER} preset="fixed" backgroundColor={color.transparent}>

          {/* Header */}
          <Header
            headerTx="demoListScreen.title"
            leftIcon="back"
            onLeftPress={goBack}
            style={HEADER}
            titleStyle={HEADER_TITLE}
          />

          {/* Search */}
          <View style={INPUT_CONTAINER}>
            <TextInput
              style={TEXT_INPUT}
              placeholderTextColor={"#FFFFFF"}
              placeholder="Search...."
              value={searchText}
              onChangeText={text=>{setSearchText(text); search(searchText); }}
            />
            {/* <FontAwesome
              style = {{flex: 1, fontSize: 18 , color: "#FFFFFF"}}
              name="search"
              size={18}
              onPress={() => TK(searchText)}
            /> */}
          </View>

          {/* Flat List */}
          <View style={{flex: 20, paddingTop: spacing[4]}}>
          <Animated.FlatList
            contentContainerStyle={FLAT_LIST}
            data={search(searchText)}

            onScroll={Animated.event(
              [{nativeEvent: {contentOffset:{y: scrollY}}}],
              {useNativeDriver: true}
            )}

            keyExtractor={(item) => String(item.id)}
            renderItem={({ item, index }) => {
              // Animation
              const inputRange = [
                -1, // at -1 and below is going to remain the same
                0, // at 0 and below is going to remain the same
                ITEM_SIZE *index, // top of the animation ( animation when it reaches the top edge)
                ITEM_SIZE *(index + 1 ), // when this animation going ro finish ( 1 different item)
              ]

              const scale = scrollY.interpolate({
                inputRange,
                outputRange:[1,1,1,0]} // 0: animation start 
              )
                
              return(
              <TouchableOpacity onPress={() => {ListDetail(item)}}> 
              <Animated.View style={{...LIST_CONTAINER, transform:[{scale}]}}>
                <Image source={{ uri: item.image }} style={IMAGE} />
                <Text style={LIST_TEXT}>
                  {item.name} ({item.status})
                </Text>
              </Animated.View>
              </TouchableOpacity>
              
              )
            }}
          />
          </View>
        </Screen>
      </View>
    )
  },
)
